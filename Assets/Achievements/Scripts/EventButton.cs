﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EventButton : MonoBehaviour, IPointerDownHandler {

    public GameEvent eventType;
    public int eventValue = 1;

    void Start()
    {
        GetComponentInChildren<Text>().text = eventType.ToString();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        EventManager.TriggerEvent(eventType, new Dictionary<string, object>() {
            { "value", eventValue }
        });
    }
}
