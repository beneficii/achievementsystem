﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementUINode : MonoBehaviour {

    [HideInInspector] public AchievementNode node;

    public Text DescriptionText;
    public Text ProgressText;

    public Button CollectButton;

    private void Start()
    {
        CollectButton.interactable = false;
        DescriptionText.text = node.Description;
        UpdateProgress(0);
        node.UpdateScoreAction = UpdateProgress; //separating logic from UI
    }

    public void UpdateProgress(int progress)
    {
        ProgressText.text = Mathf.Min(progress, node.goal).ToString() + "/" + node.goal.ToString();
        if(progress>= node.goal)
        {
            CollectButton.interactable = true;
        }
    }

    public void Collect()
    {
        Debug.Log("Collected " + node.reward);
        Destroy(gameObject);
        //GetComponentInParent<DummyBranchScript>().AchievementCollected();// done in update of parent cus destroy delays
    }
}
