﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public enum GameEvent
{
    PressButtonA,
    PressButtonB,
    PressButtonC
}

public class JSONEvent: UnityEvent<Dictionary<string,object>> {}

public class EventManager : MonoBehaviour
{
    public delegate bool EventFunction();

    private Dictionary<int, JSONEvent> eventDictionary;

    private static EventManager eventManager;

    public static EventManager Instance
    {
        get
        {
            if (!eventManager)
            {
                eventManager = FindObjectOfType(typeof(EventManager)) as EventManager;

                if (!eventManager)
                {
                    Debug.LogError("No EventManager found!");
                }
                else
                {
                    eventManager.Init();
                }
            }

            return eventManager;
        }
    }

    void Init()
    {
        if (eventDictionary == null)
        {
            eventDictionary = new Dictionary<int, JSONEvent>();
        }
    }

    public static void StartListening(GameEvent eventType, UnityAction<Dictionary<string, object>> listener)
    {
        JSONEvent thisEvent = null;
        if (Instance.eventDictionary.TryGetValue((int)eventType, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new JSONEvent();
            thisEvent.AddListener(listener);
            Instance.eventDictionary.Add((int)eventType, thisEvent);
        }
    }

    public static void StopListening(GameEvent eventType, UnityAction<Dictionary<string, object>> listener)
    {
        if (eventManager == null) return;
        JSONEvent thisEvent = null;
        if (Instance.eventDictionary.TryGetValue((int)eventType, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    public static void TriggerEvent(GameEvent eventType, Dictionary<string, object> dictionary)
    {
        JSONEvent thisEvent = null;
        if (Instance.eventDictionary.TryGetValue((int)eventType, out thisEvent))
        {
            thisEvent.Invoke(dictionary);
        }
    }
}
