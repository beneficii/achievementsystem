﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class AchievementController : MonoBehaviour {

    public TextAsset DataFile;
    public TextAsset UserFile;
    List<AchievementBranch> branches;

    public GameObject BranchPanelPrefab;

    private void Awake()
    {
        // Load Data
        branches = new List<AchievementBranch>();
        JSONObject data = new JSONObject(DataFile.text); // ToDo: take this from server
        foreach (var json in data.list)
        {
            branches.Add(new AchievementBranch(json));
        }
        
        JSONObject userdata = new JSONObject(UserFile.text); // ToDo: take this from server
        foreach (var json in userdata.list)
        {
            foreach (var branch in branches)
            {
                if(branch.Id == json.GetField("id").str)
                {
                    branch.Update(json);
                }
            }
        }


        // Create UI
        foreach (var branch in branches)
        {
            Instantiate(BranchPanelPrefab, transform).GetComponent<AchievementBranchUINode>().node = branch;
        }
    }

    void OnEnable () {
        EventManager.StartListening(GameEvent.PressButtonA, ReportButtonA);
        EventManager.StartListening(GameEvent.PressButtonB, ReportButtonB);
        EventManager.StartListening(GameEvent.PressButtonC, ReportButtonC);
    }

    void OnDisable()
    {
        // There might be memory leaks if we don't do this
        EventManager.StopListening(GameEvent.PressButtonA, ReportButtonA);
        EventManager.StopListening(GameEvent.PressButtonB, ReportButtonB);
        EventManager.StopListening(GameEvent.PressButtonC, ReportButtonC);
    }

    private void ReportButtonA(Dictionary<string, object> dictionary)
    {
        OnAddPoints("ButtonA", (int)dictionary["value"]);
    }

    private void ReportButtonB(Dictionary<string, object> dictionary)
    {
        OnAddPoints("ButtonB", (int)dictionary["value"]);
    }

    private void ReportButtonC(Dictionary<string, object> dictionary)
    {
        OnAddPoints("ButtonC", (int)dictionary["value"]);
        OnAddPoints("ButtonA", 1);
    }

    private void OnAddPoints(string type, int amount)
    {
        foreach(var branch in branches)
        {
            branch.TryIncreaseResource(type, amount);
        }
    }
}
