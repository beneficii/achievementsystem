﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AchievementBranchUINode : MonoBehaviour {

    public Text NameText;
    [HideInInspector]public AchievementBranch node;

    public GameObject AchievementPanelPrefab;

	// Use this for initialization
	void Start () {
        NameText.text = node.Id;
        InitNodes();
        StartCoroutine(DelayedRefresh());
    }

    //We need this function because objects aren't instantiated immidiately
    IEnumerator DelayedRefresh()
    {
        yield return null; //We are guaranateed that node will be instantiated before Update
        node.Refresh();
    }

    public void InitNodes()
    {
        foreach (var achievement in node.CurrentAchievements)
        {
            Instantiate(AchievementPanelPrefab, transform).GetComponent<AchievementUINode>().node = achievement;
        }
    }
    
	void Update () {
        if (node != null && transform.childCount == 1)
        {
            if (!node.FinishLevel())
            {
                node = null;
                Destroy(gameObject);
            }
            else
            {
                InitNodes();
            }
        }
	}
}
