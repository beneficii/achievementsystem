﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementNode
{
    public string Id; //maybe int or other

    public int reward;
    public int goal;
    public string resource;

    //#g - goal points
    string description;
    public string Description
    {
        get
        {
            return description
                .Replace("#g", goal.ToString());
        }
    }

    public delegate void ScoreAction(int score);
    public ScoreAction UpdateScoreAction;

    public AchievementNode(JSONObject json)
    {
        Id = json.GetField("id").str;
        goal = (int)json.GetField("goal").i;
        resource = json.GetField("resource").str;
        reward = (int)json.GetField("reward").i;
        description = json.GetField("description").str;
    }

    public bool UpdateProgress(int progress)
    {
        if (UpdateScoreAction != null)
        {
            UpdateScoreAction.Invoke(progress);
        }

        return progress >= goal;
    }
}

public class AchievementBranch
{
    public string Id;
    int level;
    Dictionary<string, int> resources;
    List<List<AchievementNode>> achievements; //grouped in levels

    public AchievementBranch(JSONObject json)
    {
        this.Id = json.GetField("id").str;

        achievements = new List<List<AchievementNode>>();
        foreach (var achievementLevel in json.GetField("achievements").list)
        {
            List<AchievementNode> layer = new List<AchievementNode>();
            foreach (var node in achievementLevel.list)
            {
                layer.Add(new AchievementNode(node));
            }
            achievements.Add(layer);
        }
        level = 0;
        InitResources();
    }


    //Update with user data on Init
    public void Update(JSONObject json)
    {
        level = (int)json.GetField("level").i;
        InitResources();

        var resourceJSON = json.GetField("resources"); //workaround for dictionary defence mechanisms
        int amount;
        List<string> keys = new List<string>(resources.Keys);
        foreach (var resource in keys)
        {
            resourceJSON.GetField(out amount, resource, 0);
            resources[resource] = amount;
            CheckAchievements(resource, true);
        }
    }

    public void TryIncreaseResource(string type, int value)
    {
        if (resources.ContainsKey(type))
        {
            resources[type] += value;
            CheckAchievements(type);
        }
    }

    public void CheckAchievements(string resourceName, bool isInit = false)
    {
        //foreach (var node in CurrentAchievements)
        List<AchievementNode> current = achievements[level];
        for(int i = current.Count-1; i>= 0;i--)
        {
            var node = current[i];
            if (node.resource == resourceName)
            {
                if(node.UpdateProgress(resources[resourceName]) && isInit)
                {
                    current.RemoveAt(i);
                }
            }
        }
    }

    public void Refresh()
    {
        foreach(var node in CurrentAchievements)
        {
            node.UpdateProgress(resources[node.resource]);
        }
    }

    public bool FinishLevel()
    {
        if (level + 1 < achievements.Count)
        {
            level++;
            InitResources();
            return true;
        }
        else
        {
            return false;
        }
    }

    void InitResources()
    {
        resources = new Dictionary<string, int>();
        foreach (var node in CurrentAchievements)
        {
            resources[node.resource] = 0; //repeated resources are no problem
        }
    }

    public IEnumerable<AchievementNode> CurrentAchievements
    {
        get
        {
            List<AchievementNode> list;
            try
            {
                list = achievements[level];
            }
            catch (IndexOutOfRangeException e)
            {
                Debug.LogError("Error in achievement structure: " + e.Message);
                yield break; //this should not happen, however we don't want to break stuff
            }

            foreach (var item in list)
            {
                yield return item;
            }
        }
    }
}
