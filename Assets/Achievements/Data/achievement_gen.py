import random
import json

all_resources = ["ButtonA", "ButtonB", "ButtonC"]

class Achievement(object):
	"""docstring for achievement"""
	def __init__(self, ID):
		super(Achievement, self).__init__()
		self.ID = ID
		self.resource = random.choice(all_resources)
		self.goal = random.randint(1, 10)
		self.reward = random.randint(5, 10)
		self.description = "Collect #g " + str(self.resource)

	def game_obj(self):
		return {
               "id":self.ID,
               "resource":self.resource,
               "goal":self.goal,
               "reward":self.reward,
               "description":self.description
            }


class Branch(object):
	"""docstring for Branch"""
	def __init__(self, ID):
		super(Branch, self).__init__()
		self.ID = ID
		self.achievements = []
		n = 0
		for i in range(3):
			layer = []
			for j in range(random.randint(1, 3)):
				layer += [Achievement("ach"+str(n)).game_obj()]
				n+= 1
			self.achievements += [layer]

	def game_obj(self):
		return {
               "id":self.ID,
               "achievements":self.achievements
            }

	def user_obj(self):
		return {
               	"id":self.ID,
               	"level":0,
               	"resources": {
               		all_resources[0]:1,
               		all_resources[1]:2,
               		all_resources[2]:3
               	}
            }

def main():
	all_branches = ["Attack","Money","Lulz"]
	#data_g = {ID:Branch(ID).game_obj() for ID in all_branches}
	data_g = [Branch(ID).game_obj() for ID in all_branches]
	data_u = [Branch(ID).user_obj() for ID in all_branches]

	with open('achievements_game.json', "w") as f:
		f.write(json.dumps(data_g, indent=4, sort_keys=True))

	with open('achievements_user.json', "w") as f:
		f.write(json.dumps(data_u, indent=4, sort_keys=True))

main()